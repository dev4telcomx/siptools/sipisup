#!/usr/bin/python3
#
# Dev4Telco | https://dev4telco.mx | ping@dev4telco.mx
#
import os
import re
import sys
import time
import signal
import socket
import logging as log
import subprocess

from lib.sipping import SipPing
from lib import config as cnf

log.basicConfig(format='%(asctime)s - %(message)s',level=log.DEBUG)

interval    = cnf.INTERVAL
timeout     = cnf.TIMEOUT
retries     = cnf.RETRIES

results     = []
failures    = dict
fails       = 0
start_time  = time.time()
target_file = "./targets.txt"
logs_file   = "./ping.logs"

def handle_sigint(sig, frame):
    show_stats()
    sys.exit(0)

def show_stats():
    end_time = time.time()
    num_received = 0
    num_timed_out = 0
    total_latency = 0
    min_latency = (timeout * 1000) + 1
    max_latency = 0
    for result in results:
        if (result is None):
            num_timed_out = num_timed_out + 1
        else:
            num_received = num_received + 1
            total_latency = total_latency + result

            if (result < min_latency):
                min_latency = result

            if (result > max_latency):
                max_latency = result

    packetloss_percentage = (num_timed_out / len(results)) * 100

    if (num_received > 0):
        # calculate the avg latency
        avg_latency = total_latency / num_received
    time_result = round((end_time - start_time) * 1000, 3)

    #myrol = takedown_iface(floating_iface)
    #log.info(f"[stats] --- {floating_ip}:{floating_ip_port} ping statistics ---")
    log.info(f"[stats] {len(results)} packets transmitted, {num_received} packets received, {packetloss_percentage}% packet loss")

    if (num_received > 0):
        log.debug(f"[stats] round-trip min/avg/max = {round(min_latency*1000,3)}/{round(avg_latency*1000,3)}/{round(max_latency*1000,3)} ms")
    log.info(f"[service] sipalive stopped!")

# MAIN

log.info(f"[service] sipiup started!")
signal.signal(signal.SIGINT, handle_sigint)     # catch ctrl-c
signal.signal(signal.SIGTERM, handle_sigint)    # catch systemctl stop

def ip_validator(ip):
    #print(f"Validating: {ip}")
    if ( re.match("\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}",ip) ):
        #print(re.match("\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}",ip))
        return True
    else:
        #print(re.match("\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}",ip))
        return False


flogs       = open(logs_file,"w+")
ip          = ""
port        = ""
while (True):
    ftargets    = open(target_file,"r")
    for item in ftargets:
        #print(f"Trying item: {item}")
        target = item.rstrip()
        ipIsValid = False
        try:
            #print(target.split(":"))
            (ip,port) = target.split(":")
            #print(f"is valid ip? {ip}:{port}")
            #ipIsValid = ip_validator(ip, port, "", "", "udp", timeout)
            ipIsValid = ip_validator(ip)
            #print(f"is valid ip? |{ip}:{port}|{ipIsValid}|")
        except:
            #print(f"target ip: |{target}|")
            pass
        if( ipIsValid ):

            sipping     = SipPing(ip, int(port))
            #sipping     = SipPing(ip, int(port), "", "", "udp", int(timeout))
            #ipIsValid = ip_validator(ip, port, "", "", "udp", timeout)
            ping_result = sipping.ping_once()
            if (ping_result is None):
                flogs.write(f"{ip} {port} 0")
                log.info(f"[options] Reply from {ip}:{port}\t time=timeout")
                #failures[str(ip)] = failures[str(ip)] + 1
            else:
                flogs.write(f"{ip} {port} {round(ping_result*1000, 3)}")
                log.debug(f"[options] Reply from {ip}:{port}\t time={round(ping_result*1000, 3)} ms")
                failures=0
        else:
            pass
    ftargets.close
    #print(f"sleeping {interval} seconds")
    time.sleep(interval)
