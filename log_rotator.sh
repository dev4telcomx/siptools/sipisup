#!/bin/bash
#
# Dev4Telco | https://dev4telco.mx | ping@dev4telco.mx
#
timestamp=$(date '+%Y%m%d_%H%M%S')
hostname=$(hostname)
log_file=$hostname"_sipoptions.log"
backup_file=$hostname"_sipoptions_"$timestamp".log"

systemctl stop sipisup
mv /usr/local/sipisup/$log_file /usr/local/sipisup/$backup_file
systemctl start sipisup
bzip2 /usr/local/sipisup/$backup_file
