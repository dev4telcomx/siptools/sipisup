#!/bin/bash 
#
# Dev4Telco | https://dev4telco.mx | ping@dev4telco.mx
#
work_dir="/usr/local/sipisup"
hostname=$(hostname)
log_file=$hostname"_sipoptions.log"

#mkdir $work_dir
#cp -frv * $work_dir
#cd $workdir

echo "sipisup installer! (tested on Debian12 bookworm)"

echo "  - Installing sipisup dependencies packages! (net-tools rsyslog python3-decouple)"
apt install -y net-tools rsyslog python3-decouple

echo "  - Enabling kernel support to bind a floating ip onfly!"
sysctl -w "net.ipv4.ip_nonlocal_bind"=1 > /dev/null

echo "  - Enabling kernel support to bind a floating ip, making persistent!"
echo "net.ipv4.ip_nonlocal_bind = 1" >> /etc/sysctl.conf

echo "  - Registering service in systemctl!"
cat << EOF > /etc/systemd/system/sipisup.service
[Unit]
Description=sipisup
After=syslog.target
After=network.target

[Service]
WorkingDirectory=$work_dir
Type=simple
User=root
Group=root
ExecStart=/usr/bin/python3 -u $work_dir/sipisup.py
StandardOutput=append:$work_dir/$log_file
StandardError=inherit

# Give the script some time to startup
TimeoutSec=300

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload


echo "  - Configuring log rotation command!"
echo "59 23 	* * * 	root	$work_dir/./log_rotator.sh" >> /etc/crontab

echo "  - Making scripts executable!"
chmod +x log_rotator.sh
chmod +x sipisup.py

echo ""
echo ""
echo "  - Now sipisup service deployed, please configure .env file before start the service, (please look in .env.master .en.slave examples)"
echo "  - you can use: systemctl [start|stop|status] sipisup "
