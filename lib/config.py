from decouple import config, Config, RepositoryEnv
env_config  = Config(RepositoryEnv('/usr/local/sipisup/.env'))
INTERVAL    = int(config("INTERVAL"))
TIMEOUT     = int(config("TIMEOUT"))
RETRIES     = int(config("RETRIES"))
